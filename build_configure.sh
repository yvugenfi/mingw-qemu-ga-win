QEMU_GA_MANUFACTURER="RedHat" QEMU_GA_DISTRO="RHEL" QEMU_GA_VERSION="103.0.0" ./configure \
    --disable-docs \
    --enable-guest-agent \
    --disable-zlib-test \
    "$@"
